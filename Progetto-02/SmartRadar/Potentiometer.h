#ifndef __POTENTIOMETER__
#define __POTENTIOMETER__

class Potentiometer{ 
public:
  Potentiometer(int pin);  
  void readDistance(int tmin, int tmax);

private:
  int pin;
};

#endif
