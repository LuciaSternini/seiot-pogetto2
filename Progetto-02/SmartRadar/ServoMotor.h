#ifndef __SERVOMOTOR__
#define __SERVOMOTOR__
#include <Servo.h>

class ServoMotor {

public:
  ServoMotor(int pin);

  void on();
  void setPosition(int angle);
  void off();
    
private:
  int pin; 
  Servo motor; 
};

#endif
