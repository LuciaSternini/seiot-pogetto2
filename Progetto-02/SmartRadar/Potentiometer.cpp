#include "Potentiometer.h"
#include "Arduino.h"

Potentiometer::Potentiometer(int pin){
  this ->pin = pin;
}

void Potentiometer::readDistance(int tmin, int tmax){
  map(analogRead(pin), 0, 1023, tmin, tmax);  
}
