#ifndef __PIR__
#define __PIR__

class Pir{ 
public:
  Pir(int pin);  

private:
  int pin;
};

#endif
