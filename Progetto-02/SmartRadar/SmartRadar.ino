#include "Led.h"
#include "Button.h"

#define PIN_LED_D 12
#define PIN_LED_A 13
#define POT_PIN A0
#define SERVO_PIN 5
#define TRIG_PIN 8 //sonar
#define ECHO_PIN 7 //sonar
#define PIR_PIN 4
#define PIN_TM_1 1 
#define PIN_TM_2 2
#define PIN_TM_3 3
#define NUM_BUTTON 3

const int tmin = 2;
const int tmax = 10;
const int directions = 16;
const float dNear = 0.2;
const float dFar = 0.2;

Led* ledA, ledD;
Button *buttonList[NUM_BUTTON];

void setup() {
  // put your setup code here, to run once:
  ledA = new Led(PIN_LED_A);
  ledD = new Led(PIN_LED_D);
  buttonList[0] = new Button(PIN_TM_1); 
  buttonList[1] = new Button(PIN_TM_2);
  buttonList[2] = new Button(PIN_TM_3); 
  
}

void loop() {
  // put your main code here, to run repeatedly:

}
